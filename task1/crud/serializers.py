from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name','email', 'password', 'phone']

    
    # name = serializers.CharField(max_length=100)
    # email = serializers.CharField(max_length=100)
    # password = serializers.CharField(max_length=100)
    # phone = serializers.CharField(max_length=100)