from django.urls import path
from crud .views import *

urlpatterns = [ 
    # Get all Details
    path('user/', api_all_detail_user_view, name='get_all_user_details'),
    # Get details of specific user
    path('user/<int:id>', api_specific_detail_user_view, name='get_specific_user_details'),
    # Create a user
    path('user/', api_user_detail_post, name='post_user_details'),
    # Update a user
    path('user/<int:id>', api_user_detail_update, name='update_user_details'),
    # Delete a user
    path('user/<int:id>', api_user_detail_delete, name='delete_user_details'),
]
