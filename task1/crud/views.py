# from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from crud .models import User
from crud .serializers import UserSerializer

# Create your views here.

@api_view(['GET', ])
# Get details of a specific user
def api_specific_detail_user_view(request, id):
    try:
        user_details = User.objects.get(id=id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = UserSerializer(user_details)
    return Response(serializer.data)


@api_view(['GET', ])
# Get details of all the users
def api_all_detail_user_view(request):
    try:
        user_details = User.objects.all().order_by('id')
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    serializer = UserSerializer(user_details, many=True)
    return Response(serializer.data)


@api_view(['POST', ])
# Post al the details of the user
def api_user_detail_post(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT', ])
# Update the details of the user 
def api_user_detail_update(request, id):
    try:
        user_details = User.objects.get(id=id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = UserSerializer(user_details, data=request.data, partial=True)
    data={}
    if serializer.is_valid():
        serializer.save()
        data['success'] = 'update successful'
        return Response({"status": "success", "data": serializer.data})
    return Response({"status": "failure", "error": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


    
@api_view(['DELETE' ])
# Delete a specific user
def api_user_detail_delete(request, id):
    try:
        user_details = User.objects.get(id=id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_400_NOT_FOUND)

    operation = user_details.delete()
    data={}
    if operation:
        data["success"] = "deletion successful"
    else:
        data["failure"] = "deletion failed"
    return Response(data=data)
        
